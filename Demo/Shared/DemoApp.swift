//
//  DemoApp.swift
//  Shared
//
//  Created by rajesh.sarani on 04/08/21.
//

import SwiftUI

@main
struct DemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
